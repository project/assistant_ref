<?php

/**
 * @file
 * Solace Node Reference node_clone module integration. 
 */

/**
 * Implementation of hook_clone_node_alter().
 */
function assistant_ref_clone_node_alter(&$node, $original_node, $method) {
  // In all cases, we need to remove the fields content. There is no need to
  // check for $method parameter, because we will rely on hook_nodeapi() to
  // save our stuff.
  if (!isset($node->assistant)) {
    $node->assistant = array();
  }
  foreach (assistant_ref_enabled_fields($node, FALSE) as $field_name => $options) {
    // Empty field content.
    $node->{$field_name} = array();
    // Load settings from original node.
    if (!isset($node->assistant[$field_name])) {
      $node->assistant[$field_name] = assistant_ref_node_load($field_name, $original_node);
    }
    // Also, disable the field, per default.
    $node->assistant[$field_name]->enabled = FALSE;
  }
}
