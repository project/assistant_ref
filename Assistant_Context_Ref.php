<?php

/**
 * @file
 * Solace Node Reference custom context implementation.
 */

class Assistant_Context_Ref extends Assistant_Context_Node
{
  /**
   * Row from assistant_ref_node table.
   * 
   * @var array
   */
  protected $_settings;

  /**
   * Get row from assistant_ref_node table.
   * 
   * @return object
   *   Row from assistant_ref_node table.
   */
  public function getSettings() {
    if (empty($this->_settings)) {
      throw new Assistant_Context_Exception('Missing settings in context, did you use the setContext() method?');
    }
    return $this->_settings;
  }

  /**
   * Get row from assistant_ref_node table.
   * 
   * @param object $settings
   *   Row from assistant_ref_node table.
   */
  public function setSettings(&$settings) {
    if (empty($settings) || !isset($settings->fid) || !isset($settings->field_name) || !isset($settings->nid)) {
      throw new Assistant_Context_Exception('Incomplete settings given');
    }
    $this->setNid($settings->nid);
    $this->_settings = $settings;
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_ContextAbstract#getModule()
   */
  public function getModule() {
    return 'assistant_ref';
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_ContextAbstract#allowPreview()
   */
  public function allowPreview() {
    return TRUE;
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_ContextAbstract#allowSave()
   */
  public function allowSave() {
    return TRUE;
  }

  /**
   * (non-PHPdoc)
   * @see Assistant_ContextAbstract#getFid()
   */
  public function getFid() {
    return $this->_settings->fid; 
  }
}
