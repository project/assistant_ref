<?php

/**
 * @file
 * Solace Node Reference hooks descriptions.
 */

/**
 * This hook is fired when assistant feature settings are deleted for a node.
 * 
 * @param object $node
 *   Modified node
 */
function hook_assistant_ref_node_delete($node) {
  // Do you stuff here
}

/**
 * This hook is fired when assistant feature settings are modified for a node.
 * 
 * Important notice, it can enable or disable settings for this node, you'll
 * have to check by yourself what happened. Note that when this will happen,
 * filter may have or may not have been changed.
 * 
 * For some code samples, look at queueing and deamon submodules.
 * 
 * @param object $node
 *   Modified node
 * @param object $settings
 *   Assistant settings row, including all filters
 */
function hook_assistant_ref_node_save($node, $settings) {
  // Do you stuff here
}

/**
 * This hook is fired right after the assistant feature was definitely deleted
 * for the given field and when options are removed.
 * 
 * @param string $field_name
 *   Field name
 */
function hook_assistant_ref_field_detach($field_name) {
  // Do your stuff here
}

/**
 * This hook is fired right after the assistant feature has been attached to a
 * field.
 * 
 * Important notice: it can be an update too, you'll have to check it by
 * yourself.
 * 
 * @param string $field_name
 *   Field name
 * @param array $options
 *   Associated site wide options
 */
function hook_assistant_ref_field_attach($field_name, $options) {
  // Do your stuff here
}

/**
 * This hook is fired right after the assistant feature was disabled for the
 * given field.
 * 
 * @param string $field_name
 *   Field name
 */
function hook_assistant_ref_field_disable($field_name) {
  // Do your stuff here
}

/**
 * This hook is fired right after the assistant feature was enabled for the
 * given field.
 * 
 * @param string $field_name
 *   Field name
 */
function hook_assistant_ref_field_enable($field_name) {
  // Do your stuff here
}

/**
 * Fired when displaying informations about content to its owner user.
 * 
 * @param object $settings
 *   Assistant settings row, including all filters and node
 * 
 * @return array
 *   Array of (x)html outputs, each one is a message to display
 */
function hook_assistant_ref_node_status($settings) {
  return array(t("This node is cool!"));
}
