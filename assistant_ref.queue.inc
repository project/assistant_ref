<?php

/**
 * @file
 * Solace Node Reference queue callbacks
 */

define('SCORE_MAX', 10);

/**
 * 
 * @param unknown_type $time
 * @param unknown_type $settings
 * @param unknown_type $data
 * @return unknown_type
 */
function assistant_ref_callback_ref_update($time, $settings = NULL, $data = NULL) {

  // Check if treatment is enabled for this node
  if ($settings && $settings->enabled == 1) {
    $context = _assistant_ref_context($settings->field_name, $settings->nid);
    $step    = round((1 / $settings->preset_max_list_size) * SCORE_MAX);
    $service = assistant_api_solr_get_service();
    if ($service) {
      $response = $service->searchUsingFilters($settings->fid, $context, $settings->preset_max_new_item);
      if ($response) {
        $list = $response->results;
      }
    }

    // Check if elements found
    if (isset($list) && !empty($list)) {
      $node  = node_load($settings->nid);
      $field = &$node->{$settings->field_name};
      
      $nid_exists = array();
      
      // Store existing nid to check duplicate
      foreach ($field as $noderef) {
        if (assistant_debug()) {
          print("[DEBUG] nid presents {$noderef['nid']}\n");
        }
        
        $nid_exists[$noderef['nid']] = 1;
      }

      // Loop on each received element
      foreach ($list as $result_id => $result) {
        if (assistant_debug()) {
          printf("[DEBUG] nid received {$result['nid']}\n");
        }
        
        // Check if this element is already present
        if (array_key_exists($result['nid'], $nid_exists)) {
          
          if (assistant_debug()) {
            print("[DEBUG] key {$result['nid']} already exists\n");
          }
          
          unset($list[$result_id]);
          continue;
        }
        
        // Check minimum score 
        if ($result['score'] < $settings->preset_min_score) {
          unset($list[$result_id]);
          continue;
        }
        
        // Determine score value inverted for table
        $score = SCORE_MAX - round($result['score'] * SCORE_MAX);
        $pos = round($score / $step) - 1;
        
        // Adding the nid to the noderef CCK field
        assistant_ref_add_node($field, $result['nid'], $pos, $settings->preset_max_list_size);
      }
      
      // Drop empty element
      while(TRUE) {
        if (empty($field[0]['nid']))
          array_shift($field);
        else 
          break;
      }
      
      // Reduce table to defined size
      $field = array_slice($field, 0, $settings->preset_max_list_size);
      
      // Saving node
      node_save($node);
      
    }

    // Add new treatment element
    if (count($list) > 0) {
      // Adding new search in queue
      assistant_ref_queue_enqueue('ref_update', time() + (count($list) * $settings->preset_min_time_between * variable_get(ASSISTANT_REF_WAIT_FACTOR, 1)), $settings);
      
      if (assistant_debug()) {
        print("[DEBUG] Treated element : ".count($list)."\tWait in second : ".(count($list) * $settings->preset_min_time_between * variable_get(ASSISTANT_REF_WAIT_FACTOR, 1))."\n");
      }
    } else {
      // Adding new search in queue
      assistant_ref_queue_enqueue('ref_update', time() + (variable_get(ASSISTANT_REF_WAIT_FACTOR, 1) * variable_get(ASSISTANT_REF_MIN_TIMEWAIT_NAME, ASSISTANT_REF_MIN_TIMEWAIT)), $settings);
      
      if (assistant_debug()) {
        print("[DEBUG] Treated element : ".count($list)."\tWait in second : ".variable_get(ASSISTANT_REF_MIN_TIMEWAIT_NAME, ASSISTANT_REF_MIN_TIMEWAIT) * variable_get(ASSISTANT_REF_WAIT_FACTOR, 1)."\n");
      }  
    }
    
    // Store last time execute
    variable_set(ASSISTANT_REF_DAEMONCLI_LAST_EXECUTE, time());
  }
}

/**
 * Add the new nid in the noderef element
 * @param array $array_node
 * @param int $nid
 * @param int $pos
 * @param int $max_element
 * @return none
 */
function assistant_ref_add_node(&$array_node, $nid, $pos, $max_element) {
  // Move existing elements if needed to add new element
  if (isset($array_node[$pos])) {
    for($count = $max_element - 1 ; $count >= $pos ; $count-- ) {
      $array_node[$count + 1] = $array_node[$count];
    }
  } 
  
  $array_node[$pos] = array("nid" => $nid);
}
