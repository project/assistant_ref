<?php

/**
 * @file
 * Solace Node Reference Queue API.
 */

define('ASSISTANT_REF_QUEUE_STATUS_ERROR', -1);
define('ASSISTANT_REF_QUEUE_STATUS_DONE', 0);
define('ASSISTANT_REF_QUEUE_STATUS_ADDED', 1);
define('ASSISTANT_REF_QUEUE_STATUS_WORKING', 2);

/**
 * Add a task to the task list.
 * 
 * @param string $op
 *   Internal op callback identifier
 * @param int $time
 *   Unix timestamp, when to run this task, timestamp must be accorded to system
 *   time default configuration.
 * @param object $settings = NULL
 *   (optional) Assistant table row from database describing node and field
 *   @see assistant_ref_node_load()
 * @param mixed $data = NULL
 *   (optional) Custom data that will be serialized and stored, then restitued
 *   to operation callback at run time
 * 
 * @return object
 *   New task object, with tid set, for convenience
 *   NULL in case of failure
 */
function assistant_ref_queue_enqueue($op, $time, $settings = NULL, $data = NULL) {

  if (! $description = assistant_ref_queue_operation_get($op)) {
    // TODO watchdog, operation does not exists
    return;
  }

  else if ($time < time()) {
    // TODO watchdog, task in the past
    return;
  }

  else {
    $task = new stdClass();
    $task->op = (string) $op;
    $task->time = (int) $time;
    $task->status = ASSISTANT_REF_QUEUE_STATUS_ADDED;

    if ($settings) {
      $task->field_name = (string) $settings->field_name;
      $task->nid = (int) $settings->nid;
    }

    if ($data) {
      $task->data = $data;
    }

    drupal_write_record('assistant_ref_queue', $task);

    return $task;
  }
}

/**
 * Load an object from queue.
 * 
 * @param int $tid
 *   Unique task id.
 * 
 * @return object
 *   Row from database, with 'settings' property properly loaded using
 *   assistant_ref api.
 */
function assistant_ref_queue_load($tid) {
  if ($task = db_fetch_object(db_query("SELECT * FROM {assistant_ref_queue} WHERE tid = %d", array($tid)))) {
    $task->settings = assistant_ref_node_load($task->field_name, $task->nid);
    $task->data = unserialize($task->data);
    return $task;
  } else {
    return NULL;
  }
}

/**
 * Find object in queue matching the given filter. If no filter is given, for
 * performance reasons, we will return an empty array.
 * 
 * @see assistant_ref_queue_load()
 * 
 * @param array $filter
 *   Key/value pairs, keys are some column of task table, values can be any
 *   mixed values that will match the corresponding table column.
 * @param array $options = array()
 *   Key/value pairs to control query, valid keys are:
 *     - 'limit' : (default 0) Set query result limit, if 0 given, then no
 *       limit is set
 *     - 'order by' : (default time) Set default order field
 *     - 'order' : (default asc) Set default order
 * 
 * @return array
 *   Key/value pairs. Keys are task tid, values are corresponding task object
 *   loaded with assistant_ref_queue_load().
 */
function assistant_ref_queue_find($filter, $options = array()) {
  $ret = array();

  $where = array();
  $args = array();

  foreach ($filter as $key => $value) {
    switch ($key) {

      case 'tid':
        $where[] = "tid = %d";
        $args[] = $value;
        break;

      case 'field_name':
        $where[] = "field_name = '%s'";
        $args[] = $value;
        break;

      case 'nid':
        $where[] = "nid = %d";
        $args[] = $value;
        break;

      case 'op':
        $where[] = "op = '%s'";
        $args[] = $value;
        break;

      case 'status':
        $where[] = "status = %d";
        $args[] = $value;
        break;

      default:
        watchdog('assistant_ref_queue', 'assistant_ref_queue_find(): ' . print_r($key, TRUE) . ': wrong filter', NULL, WATCHDOG_ERROR);
        break;
    }
  }

  if (empty($where)) {
    watchdog('assistant_ref_queue', 'assistant_ref_queue_find(): no filter given', NULL, WATCHDOG_ERROR);
    return array();
  }

  $args[] = (isset($options['order by']) ? $options['order by'] : 'time');
  $args[] = (isset($options['order']) ? $options['order'] : 'asc');
  $limit = (isset($options['limit']) ? $options['limit'] : 0);

  $query = "SELECT tid FROM {assistant_ref_queue} WHERE " . implode(' AND ', $where) . " ORDER BY %s %s";
  if ($limit > 0) {
    $query .= " LIMIT %d";
    $args[] = $limit;
  }

  $result = db_query($query, $args);
  while ($tid = db_result($result)) {
    $ret[$tid] = assistant_ref_queue_load($tid);
  }

  return $ret;
}

/**
 * Dequeue next task and execute callback. This will not break on fail, and
 * will continue until it finds a valid task to execute.
 * 
 * @return boolean
 *   TRUE if queue is not empty
 *   FALSE if queue is empty
 */
function assistant_ref_queue_dequeue() {
  $time = time();

  while(TRUE) {
    // Transaction lock handling so multiple workers won't get the same task
    // Note that with MyISAM MySQL engines, begin, select for update and commit
    // statements will just be ignored.
    // TODO: this need to be tested with both psql and mysql
    db_query("BEGIN");
    db_query("SELECT * FROM {assistant_ref_queue} WHERE status = %d AND time < %d FOR UPDATE", array(ASSISTANT_REF_QUEUE_STATUS_ADDED, $time));
    $tid = db_result(db_query("SELECT tid FROM {assistant_ref_queue} WHERE status = %d AND time < %d ORDER BY time ASC LIMIT 1", array(ASSISTANT_REF_QUEUE_STATUS_ADDED, $time)));
    db_query("UPDATE {assistant_ref_queue} SET status = %d WHERE tid = %d", array(ASSISTANT_REF_QUEUE_STATUS_WORKING, $tid));
    db_query("COMMIT");

    if (db_error()) {
      // DB error will happen if transaction fails
      watchdog("asistant_ref_queue", "[Queue_dequeue] DB_ERROR", array(), WATCHDOG_DEBUG);
      continue;
    }
    else if (! $tid) {
      // Empty queue or no task to execute.
      return TRUE;
    }
    else if (! $task = assistant_ref_queue_load($tid)) {
      db_query("UPDATE {assistant_ref_queue} SET status = %d WHERE tid = %d", array(ASSISTANT_REF_QUEUE_STATUS_ERROR, $tid));
      watchdog("asistant_ref_queue", "[Queue_dequeue] no task", array(), WATCHDOG_DEBUG);
      continue;
    }
    else if (! $description = assistant_ref_queue_operation_get($task->op)) {
      db_query("UPDATE {assistant_ref_queue} SET status = %d WHERE tid = %d", array(ASSISTANT_REF_QUEUE_STATUS_ERROR, $tid));
      watchdog("asistant_ref_queue", "[Queue_dequeue] no description", array(), WATCHDOG_DEBUG);
      continue;
    }
    else {
      // Else all is ok then
      if (assistant_debug()) {
        print("---------------------------------------\n");
        print "[DEBUG] ".date("H:i:s")." Task to execute : {$tid}\n";
      }
      $description['callback']($task->time, $task->settings, $task->data);
      db_query("UPDATE {assistant_ref_queue} SET status = %d WHERE tid = %d", array(ASSISTANT_REF_QUEUE_STATUS_DONE, $tid));

      if (assistant_debug()) {
        print "[DEBUG] ".date("H:i:s")." End execute {$tid}\n";
      }

      return TRUE;
    }
  }
}

/**
 * Load and check for filter file consistency.
 * 
 * If $description['file'] key is not present, assume that filter implmentation
 * lives in the main module file, so do not aptempt to load it and just check
 * callback exists.
 * 
 * @param array $description
 *   Full operation description, altered by assistant_ref_operation_get()
 *   function, including added callbacks descriptions.
 * 
 * @param boolean $check = FALSE
 *   Set to TRUE to activate consistency checks, those should not be usefull
 *   once the cache is set.
 * 
 * @return boolean|NULL
 *   TRUE if file loaded and callback functions exists, FALSE else
 *   NULL if consistency checks are deactivated
 */
function _assistant_ref_queue_operation_load_include(&$description, $check = FALSE) {
  if (isset($description['file'])) {
    $path = $description['file'];
    // First check module does not provide a full path
    if (! preg_match('/\//', $path)) {
      // If not, check for normal path
      $path = drupal_get_path('module', $description['module']) . '/' . $path;
    }
    // Include file if given
    if (! $check || is_file($path)) {
      require_once $path;
    }
    else {
      return FALSE;
    }
  }
  if ($check) {
    return function_exists($description['callback']);
  }
}

/**
 * Get operations list.
 * 
 * @param string $op = NULL
 *   (optional) Internal operation name
 * 
 * @return mixed
 *   - If no $op given: return a simple array of operations names
 *   - If $op is given: return only the descriptive array of given operation
 */
function assistant_ref_queue_operation_get($op = NULL) {
  static $cache = NULL, $names = array();

  // Load cache if exists
  if ($cache === NULL) {
    $cached = cache_get('assistant_ref_queue_operations', 'cache');

    if (! empty($cached->data)) {
      $cache = &$cached->data;
      $names = array_keys($cache);
    }
  }

  if ($cache === NULL) {
    $operations = array();

    foreach (module_implements('assistant_ref_queue_operations') as $module) {
      $module_operations = module_invoke($module, 'assistant_ref_queue_operations');

      foreach ($module_operations as $_op => &$description) {
        $description['module'] = $module;
        // Check file and callbacks consistency
        if (_assistant_ref_queue_operation_load_include($description, TRUE)) {
          $operations[$_op] = $description;
        }
      }
    }

    cache_set('assistant_ref_queue_operations', $operations, 'cache');
    $cache = &$operations;
    $names = array_keys($operations);
  }

  if ($op) {
    _assistant_ref_queue_operation_load_include($cache[$op], FALSE);
    return $cache[$op];
  }
  else {
    return $names;
  }
}

/**
 * Implementation of hook_assistant_ref_node_save
 * @param object $node
 * @param object $settings
 */
function assistant_ref_queue_assistant_ref_node_save($node, $settings) {
  // Erase existing element first
  db_query("DELETE FROM {assistant_ref_queue} WHERE nid = %d AND field_name = '%s' AND status = %d",
    array($settings->nid, $settings->field_name, ASSISTANT_REF_QUEUE_STATUS_ADDED));
      
  // Check if filter is enabled
  if (isset($settings->enabled) && $settings->enabled == 1) {
    assistant_ref_queue_enqueue('ref_update', time(), $settings);
  }
}

/**
 * Implementation of hook_assistant_ref_node_delete
 * @param object $node
 */
function assistant_ref_queue_assistant_ref_node_delete($node) {
  if (db_result(db_query("SELECT 1 FROM {assistant_ref_queue} WHERE nid = %d", array($node->nid)))) {
    db_query("DELETE FROM {assistant_ref_queue} WHERE nid = %d", array($node->nid));
  }
  
  return;
}

/**
 * Implementation of hook_assistant_ref_field_disable
 * @param string $field_name
 */
function assistant_ref_queue_assistant_ref_field_disable($field_name) {
  if (db_result(db_query("SELECT 1 FROM {assistant_ref_queue} WHERE field_name = '%s'", array($field_name)))) {
    db_query("DELETE FROM {assistant_ref_queue} WHERE field_name = '%s'", array($field_name));
  }
}

/**
 * Implementation of hook_daemoncli_run().
 * 
 * Notice that assistant_ref_queue_dequeue() function has the same signature
 * than this hook implemenation.
 * 
 * @see assistant_ref_queue_dequeue()
 */
function assistant_ref_queue_daemoncli_run() {
  return assistant_ref_queue_dequeue();
}
