
/**
 * @file
 * AJAX handlers for Solace Node Reference admin pages.
 */

(function ($) {

Drupal.behaviors.AssistantRefAdminMonitor = function(context) {
  $('a.assistant-ref-toggle:not(.assistant-ref-toggle-processed)', context).each(function(){
    var fieldName = this.href.substr(this.href.indexOf('#') + 1);
    new Drupal.AssistantRefAdminMonitor(this, fieldName);
    $(this).addClass('assistant-ref-toggle-processed');
  });
};

Drupal.AssistantRefAdminMonitor = function(a, fieldName) {
  // Self reference to keep object in scope in anonymous callbacks
  var self = this;
  this.fieldName = fieldName;
  this.a = a;
  this.tr = $(a).parents('tr:first');
  $(this.a).unbind('click').click(function() {
    $.ajax({
      url: Drupal.settings.AssistantRef.AssistantRefMonitor + "/" + self.fieldName,
      async: true,
      type: "GET",
      cache: false,
      success: function(data, textStatus) { self.ajaxResponse(data) },
      dataType: "json",
    });
    return false;
  });
};

Drupal.AssistantRefAdminMonitor.prototype.ajaxResponse = function(data) {
  if (this.tr.length && data.enabled != undefined) {
    if (data.enabled) {
      this.tr.removeClass('assistant-ref-disabled');
      this.tr.addClass('assistant-ref-enabled');
      $(this.a).html(Drupal.t('disable'));
    }
    else {
      this.tr.removeClass('assistant-ref-enabled');
      this.tr.addClass('assistant-ref-disabled');
      $(this.a).html(Drupal.t('enable'));
    }
  }
  // Else error happened, do not treat.
};

})(jQuery);
