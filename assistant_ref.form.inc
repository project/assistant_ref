<?php

/**
 * @file
 * Solace Node Reference node settings pages and forms.
 */

/**
 * Edit filters for node.
 * We are going to use the assistant_api_filters_form() form.
 */
function assistant_ref_node_filters_edit($field_name, $node) {
  $context = _assistant_ref_context($field_name, $node);
  return assistant_api_filters_form_build($context);
}

/**
 * Helper for time selection
 */
function _assistant_ref_time_list() {
  static $options = NULL;
  if (! $options) {
    $options = array(
      // Fibonacci sequence for minutes: seconds * 160
      180 => format_plural(3, '1 minute', '@count minutes'),
      360 => format_plural(6, '1 minute', '@count minutes'),
      540 => format_plural(9, '1 minute', '@count minutes'),
      900 => format_plural(15, '1 minute', '@count minutes'),
      // Fibonacci sequence for minutes: seconds * 3600
      3600 => format_plural(1, '1 hour', '@count hours'),
      7200 => format_plural(2, '1 hour', '@count hours'),
      10800 => format_plural(3, '1 hour', '@count hours'),
      18000 => format_plural(5, '1 hour', '@count hours'),
      28800 => format_plural(8, '1 hour', '@count hours'),
    );
  }
  return $options;
}

function _assistant_ref_range($start = 1, $stop = 10, $step = 1) {
  $ret = array();
  for ($i = $start; $i <= $stop; $i += $step) {
    $ret[(string)$i] = $i;
  }
  return $ret;
}

/**
 * Configure all fields form.
 */
function assistant_ref_node_form($form_state, $node) {
  $form = array('#tree' => TRUE, 'fields' => array());

  // Look for field candidates (could be better)
  $fields = assistant_ref_enabled_fields($node);
  
  foreach ($fields as $field_name => $options) {

    // TODO: Ugly, use content module API to get this 
    $label = db_result(db_query("SELECT label FROM {content_node_field_instance} WHERE field_name = '%s' AND type_name = '%s'", array($field_name, $node->type)));
    if (empty($label)) {
      // Fallback to field_name if no label for content type
      $label = $field_name;
    } 

    $form['nid'] = array(
      '#type' => 'value',
      '#value' => $node->nid,
    );

    if (isset($node->assistant[$field_name])) {
      $settings = $node->assistant[$field_name];
    }
    else {
      $settings = assistant_ref_node_load($field_name, $node);
    }

    // Populate subform
    $form['fields'][$field_name] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($label),
      '#collapsible' => FALSE,
      '#theme' => 'assistant_ref_node_form_field',
      // Field name storage
      'field_name' => array(
        '#type' => 'hidden',
        '#value' => $field_name
      ),
      '#attributes' => array('class' => 'ui-presets-for-field'),
    );

    if (empty($settings)) {
      $form['fields'][$field_name]['help'] = array(
        // TODO
      );      
    }

    $form['fields'][$field_name]['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable'),
      '#description' => t('If you check this option, the field will be automatically populated with content matching the configured filter.'),
      '#default_value' => (bool) (isset($settings->enabled) ? $settings->enabled : FALSE),
    );

    // Node reference and deamon settings
    $form['fields'][$field_name]['presets'] = array(
      '#type' => 'fieldset',
      '#title' => t('Presets'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attributes' => array('class' => 'assistant-dialog-main'),
      '#theme' => 'assistant_ref_columns_subform',
    );
    $form['fields'][$field_name]['presets']['preset_min_time_between'] = array(
      '#type' => 'select',
      '#title' => t('Minimum wait time'),
      '#options' => _assistant_ref_time_list(),
      '#description' => t('Minimum time to wait between two updates, when a content is added during update.'),
      '#default_value' => (int) (isset($settings->preset_min_time_between) ? $settings->preset_min_time_between : FALSE),
    );
    $form['fields'][$field_name]['presets']['preset_max_new_item'] = array(
      '#type' => 'select',
      '#title' => t('Maximum number of item to add'),
      '#options' => _assistant_ref_range(),
      '#description' => t('This is the maximum number of items which will be added into the list during each update.'),
      '#default_value' => (int) (isset($settings->preset_max_new_item) ? $settings->preset_max_new_item : FALSE),
    );
    $form['fields'][$field_name]['presets']['preset_max_list_size'] = array(
      '#type' => 'select',
      '#title' => t('Maximum list size'),
      '#options' => _assistant_ref_range(5, 30, 5),
      '#description' => t('Maximum content list size. In order to reduce this number, you have to manually remove content from list to reach the wanted limit.'),
      '#default_value' => (int) (isset($settings->preset_max_list_size) ? $settings->preset_max_list_size : FALSE),
    );
    $form['fields'][$field_name]['presets']['preset_min_score'] = array(
      '#type' => 'select',
      '#title' => t('Required score'),
      '#options' => _assistant_ref_range(0, 1, 0.1),
      '#description' => t('Minimum required score for new content in order to be added into the content list.'),
      '#default_value' => (float) (isset($settings->preset_min_score) ? $settings->preset_min_score : FALSE),
    );
    $form['fields'][$field_name]['presets']['preset_do_not_include_old'] = array(
      '#type' => 'checkbox',
      '#title' => t('Do not include old content'),
      '#description' => t("If checked, any content older than the latest added one will be ignored."),
      '#default_value' => (bool) (isset($settings->preset_do_not_include_old) ? $settings->preset_do_not_include_old : FALSE),
    );

    $throttle = ((module_exists('throttle') && throttle_status()) || FALSE);

    if (!$throttle && !empty($settings)) {
      $messages = module_invoke_all('assistant_ref_node_status', $settings);
      if (empty($messages)) {
        $status = theme('assistant_ref_node_status', array(t("This field has never been enabled or updated.")));
      }
      else {
        $status = theme('assistant_ref_node_status', $messages);
      }
      $form['fields'][$field_name]['status'] = array(
        '#type' => 'fieldset',
        '#title' => t("Status"),
        '#collapsible' => TRUE,
        '#collapsed' => empty($messages),
      );
      $form['fields'][$field_name]['status']['info'] = array(
        '#type' => 'markup',
        '#value' => $status,
      );
    }

    // Check for jQuery UI
    // TODO: temporarly deactivated because it moves form elements outside the
    // form which cause them not to be saved.
    if (FALSE /* assistant_api_use_jquery_ui() */) {
      drupal_add_js(drupal_get_path('module', 'assistant_ref') . '/assistant_ref.form.ui.js', 'module');

      $form['fields'][$field_name]['presets']['#title'] = t('Presets for @field', array('@field' => $label));
      $form['fields'][$field_name]['presets']['#collapsible'] = FALSE;
      $form['fields'][$field_name]['presets']['#collapsed'] = FALSE;
      $form['fields'][$field_name]['edit-presets'] = array(
        '#type' => 'button',
        '#value' => t('Edit presets ..'),
        '#name' => 'ui-presets-for-field-button',
        '#submit' => array(), // Disable submit on click
      );
    }

    $form['fields'][$field_name]['buttons'] = array();
    $form['fields'][$field_name]['buttons'][$field_name . '_save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#name' => 'save-' . $field_name,
      '#submit' => array('assistant_ref_node_form_single_submit'),
      '#id' => "edit-{$field_name}-edit",
    );
    $form['fields'][$field_name]['buttons']['edit'] = array(
      '#type' => 'submit',
      '#value' => t('Save and edit filters'),
      '#name' => 'edit-' . $field_name,
      '#submit' => array('assistant_ref_node_form_single_submit', 'assistant_ref_node_form_edit_submit'),
      '#id' => "edit-{$field_name}-edit",
    );
  }

  // Vertical tab usage
  if (module_exists('vertical_tabs')) {
    vertical_tabs_add_vertical_tabs($form['fields']);
  }

  if (count($fields) > 0) {
    $form['update'] = array(
      '#type' => 'submit',
      '#value' => t("Save all presets"),
    );
  }

  return $form;
}

function assistant_ref_node_form_single_submit($form, &$form_state) {
  $parts = explode('-', $form_state['clicked_button']['#name'], 1);
  $form_state['field_name'] = $parts[1];
  assistant_ref_node_form_submit($form, &$form_state);
}

function assistant_ref_node_form_edit_submit($form, &$form_state) {
  if (preg_match('/^edit-([a-zA-Z\-_]+)$/', $form_state['clicked_button']['#name'], $matches)) {
    $nid = $form_state['values']['nid'];
    $field_name = $matches[1];
    drupal_goto('node/' . $nid . '/assistant/' . $field_name, array('destination' => $_GET['q']));
  }
}

function assistant_ref_node_form_submit($form, &$form_state) {
  foreach ($form_state['values']['fields'] as $field_name => &$new_settings) {
    // Check for single value save
    if (isset($form_state['field_name']) && $form_state['field_name'] != $field_name) {
      continue;
    }
    // In case it does not exists, it's our user's first save, we have to
    // create the new object instance.
    if (! $settings = assistant_ref_node_load($field_name, $node)) {
      $settings = new stdClass();
      $settings->field_name = $field_name;
    }
    $settings->enabled = (bool) $new_settings['enabled'];
    $settings->preset_min_time_between = (int) $new_settings['presets']['preset_min_time_between'];
    $settings->preset_max_new_item = (int) $new_settings['presets']['preset_max_new_item'];
    $settings->preset_max_list_size = (int) $new_settings['presets']['preset_max_list_size'];
    $settings->preset_min_score = (float) $new_settings['presets']['preset_min_score'];
    $settings->preset_do_not_include_old = (float) $new_settings['presets']['preset_do_not_include_old'];
    // We do a copy of the 'nid' variable first, because this parameter is a
    // reference for assistant_ref_node_save(), if we pass directly pass the
    // array value, then it will be replaced with the full node which will
    // cause other submit methods to fail.
    $nid = $form_state['values']['nid'];
    assistant_ref_node_save($settings, $nid);
  }
}

function theme_assistant_ref_columns_subform($form, $cols = 2) {
  $output = '<div>';

  $percent = floor(100 / $cols - 3);
  $i = 1;

  foreach (element_children($form) as $key) {
    $output .= '<div style="width: ' . $percent . '%; float: left; margin: 1%;">' . drupal_render($form[$key]) . '</div>';
    if ($i % $cols == 0) {
      $output .= '<div class="clear-block"></div>';
    }
    $i++;
  }

  return  $output . '</div>';
}

function theme_assistant_ref_node_form_field($form) {
  // For JS
  // FIXME I think this is useless
  $form['enabled']['#id'] = 'assistant-ref-enabled-' . $form['field_name']['#value'];
  $form['enabled']['#attributes'] = array('class' => 'assistant-ref-enabled');
  $form['presets']['#prefix'] = '<div id="assistant-ref-presets-' . $form['field_name']['#value'] . '">';
  $form['presets']['#suffix'] = '</div>';
  // Empty text so the prefix and suffix show up
  $form['presets']['#value'] = '<span> </span>';
  return drupal_render($form);
}

/**
 * Invokes hook_assistant_ref_node_status() to all modules that implement it
 * and returns the global output.
 */
function theme_assistant_ref_node_status($messages) {
  $output = '<div class="assistant-ref-field-status">';
  foreach ($messages as $message) {
    $output .= $message . "<br/>";
  }
  return $output . '</div>';
}
