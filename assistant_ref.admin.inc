<?php

/**
 * @file
 * Solace Node Reference administration pages
 */

/**
 * Global settings form.
 */
function assistant_ref_admin_build_settings($form_state) {
  $form = array();

  $form[ASSISTANT_REF_MIN_TIMEWAIT_NAME] = array(
    '#type' => 'select',
    '#title' => t('Minimum wait time between two tasks'),
    '#description' => t('Time the deamon will wait between retries when the assistant could not find any content when refreshing a container.'),
    '#options' => array(
      60 => format_plural(1, '1 minute', '@count minutes'),
      120 => format_plural(2, '1 minute', '@count minutes'),
      180 => format_plural(3, '1 minute', '@count minutes'),
      360 => format_plural(6, '1 minute', '@count minutes'),
      540 => format_plural(9, '1 minute', '@count minutes'),
      900 => format_plural(15, '1 minute', '@count minutes'),
      3600 => format_plural(1, '1 hour', '@count hours'),
    ),
    '#required' => TRUE,
    '#default_value' => variable_get(ASSISTANT_REF_MIN_TIMEWAIT_NAME, ASSISTANT_REF_MIN_TIMEWAIT),
  );

  $form[ASSISTANT_REF_WAIT_FACTOR] = array(
    '#type' => 'select',
    '#title' => t('Minimum wait time modificator'),
    '#description' => t('This factor will alter deamon reactivity.<br/>When the node reference update operation do not find content to add, the deamon will enqueue a new task, this task execution time will be <strong>now + Minimum wait time * This factor</strong>.<br/>Programatically, if minimum wait time is 1 hour, and factor is 0.2, next task will run in the next 12 minutes.'),
    '#options' => array(
      '0.1' => '0.1', 
  	  '0.2' => '0.2',
      '0.5' => '0.5',
      '0.8' => '0.8',
	  '1' => t('Normal'),
    ),
    '#default_value' => variable_get(ASSISTANT_REF_WAIT_FACTOR, '1'),
  );

  return system_settings_form($form);
}

/**
 * Helper for admin pages, bootstrap needed JS and CSS.
 */
function _assistant_ref_admin_bootstrap() {
  drupal_add_js(array('AssistantRef' => array(
    'AssistantRefMonitor' => url('admin/assistant/ajax/refmonitor/toggle'),
  )), 'setting');
  drupal_add_js(drupal_get_path('module', 'assistant_ref') . '/assistant_ref.admin.js');
  drupal_add_css(drupal_get_path('module', 'assistant_ref') . '/assistant_ref.admin.css');
}

/**
 * Display all site wide configured fields page.
 */
function assistant_ref_admin_monitor_page() {
  $output = '';
  $field_types = assistant_ref_field_get_types();

  _assistant_ref_admin_bootstrap();

  $output .= '<h3>' . t('Configured fields') . '</h3>';

  $headers = array(t('Field name'), t('Content type'), t('Type'));
  $rows = array(); 
  $result = db_query("SELECT field_name FROM {assistant_ref}");

  while ($field_name = db_result($result)) {
    $options = assistant_ref_field_load($field_name);
    $field = content_fields($field_name);
    $type_name = node_get_types('name', $field['type_name']);

    $rows[] = array(
      'class' => ($options->enabled ? 'assistant-ref-enabled' : 'assistant-ref-disabled'),
      'data' => array(
        // Field human readable label
        '<strong>' . $field['widget']['label'] . '</strong> ' . theme('assistant_ref_admin_links', array(
          array(
            'title' => ($options->enabled ? t('disable') : t('enable')),
            'href' => $_GET['q'],
            'fragment' => $field_name,
            'attributes' => array('class' => 'assistant-ref-toggle ' . ($options->enabled ? 'assistant-ref-enabled' : 'assistant-ref-disabled')),
          ),
          array(
            'title' => t('nodes'),
            'href' => 'admin/build/assistant/refmonitor/active/' . $field_name,
          ),
        )),
        // Content type name, and configuration link
        $type_name . theme('assistant_ref_admin_links', array(array('title' => t('edit'), 'href' => 'admin/content/node-type/'. $field['type_name']))),
        $field_types[$options->type] . theme('assistant_ref_admin_links', array(array('title' => t('configure'), 'href' => 'admin/content/node-type/'. $field['type_name'] .'/fields/'. $field_name))),
      ),
    );
  }
  $output .= theme('table', $headers, $rows);

  return $output;
}

/**
 * AJAX callback for enable/disable field in admin pages.
 */
function assistant_ref_admin_monitor_page_ajax_toggle($options, $nid = NULL) {
  $enabled = NULL;
  if (!$nid) {
    if ($options->enabled) {
      assistant_ref_field_disable($options->field_name);
      $enabled = FALSE;
    }
    else {
      assistant_ref_field_enable($options->field_name);
      $enabled = TRUE;
    }
  }
  else if ($settings = assistant_ref_node_load($options->field_name, $nid)) {
    $settings->enabled = $enabled = !$settings->enabled;
    assistant_ref_node_save($settings, $nid);
  }
  else {
    // Some malicious people could get here.
    drupal_not_found();
    return;
  }
  print drupal_to_js(array('enabled' => $enabled));
  exit();
}

/**
 * Helper that provides a "back' button for administration pages.
 */
function assistant_ref_admin_monitor_back() {
  return array('button' => array(
    '#type' => 'submit',
    '#value' => t('Back to monitor page'),
  ));
}

/**
 * Submit handler for "back" button.
 */
function assistant_ref_admin_monitor_back_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/build/assistant/refmonitor';
}

/**
 * Display active node/field couples administration page.
 */
function assistant_ref_admin_monitor_active_page($options) {
  $output = '';

  drupal_set_title(t("Nodes for %field_name", array('%field_name' => $options->field_name)));
  _assistant_ref_admin_bootstrap();

  $query = "SELECT nid FROM {assistant_ref_node} WHERE field_name = '%s'";
  $args = array($options->field_name);

  $headers = array(t('Node'), t('Lastest update'), t('Next update'));
  $rows = array();

  $result = pager_query($query, 30, 0, NULL, $args);
  while ($nid = db_result($result)) {
    $row = array();

    // Node info
    $settings = assistant_ref_node_load($options->field_name, $nid);
    $node = node_load($settings->nid);
    $row[] = '<strong>' . check_plain($node->title) . '</strong> ' . theme('assistant_ref_admin_links', array(
      array(
        'title' => ($options->enabled ? t('disable') : t('enable')),
        'href' => $_GET['q'],
        'fragment' => $options->field_name . '/' . $node->nid,
        'attributes' => array('class' => 'assistant-ref-toggle'),
      ),
      array(
        'title' => t('presets'),
        'href' => 'node/' . $node->nid . '/assistant',
      ),
    ));

    // Latest execution
    $latest = assistant_ref_queue_find(
      array('op' => 'ref_update', 'status' => ASSISTANT_REF_QUEUE_STATUS_DONE, 'nid' => $node->nid, 'field_name' => $options->field_name),
      array('limit' => 1, 'order by' => 'time', 'order' => 'desc')
    );
    $row[] = !empty($latest) ? format_date(array_shift($latest)->time) : t('Never');

    // Next execution
    $next = assistant_ref_queue_find(
      array('op' => 'ref_update', 'status' => ASSISTANT_REF_QUEUE_STATUS_ADDED, 'nid' => $node->nid, 'field_name' => $options->field_name),
      array('limit' => 1, 'order by' => 'time', 'order' => 'asc')
    );
    $row[] = !empty($next) ? format_date(array_shift($next)->time) : t('Never');

    // Build the full row
    $rows[] = array(
      'class' => $settings->enabled ? 'assistant-ref-enabled' : 'assistant-ref-disabled',
      'data' => $row,
    );
  }
  $output .= theme('table', $headers, $rows) . theme('pager', array(), 0);

  $output .= drupal_get_form('assistant_ref_admin_monitor_back');
  return $output;
}

/**
 * Display daemon queue administration page.
 * TODO: unfinished work.
 */
function assistant_ref_admin_monitor_log_page() {
  $output = '';
  drupal_set_title(t("Queue log"));

  $output .= drupal_get_form('assistant_ref_admin_monitor_back');
  return $output;
}

/**
 * Helper in order to replace theme_links() theme function for administration
 * pages.
 */
function theme_assistant_ref_admin_links($links) {
  $_links = array();
  foreach ($links as $link) {
    if (isset($link['title']) && isset($link['href'])) {
      $_links[] = l($link['title'], $link['href'], $link);
    }
  }
  return '[' . implode(', ', $_links) . ']';
}
