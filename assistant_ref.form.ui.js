
/**
 * @file
 * jQuery UI additions for Solace Node Reference reference node form
 */

(function ($) {

Drupal.behaviors.AssistantRefFormUI = function(context) {
  $('.ui-presets-for-field', context).each(function(){
    if (! $(this).is('.assistant-ref-form-ui-processed')) {
      new Drupal.AssistantRefFormUI(this);
      $(this).addClass('assistant-ref-form-ui-processed');
    }
  });
};

Drupal.AssistantRefFormUI = function(element) {
  // Self reference to keep object in scope in anonymous callbacks
  var self = this;

  this.dialog = false;
  this.element = element;

  $('.assistant-dialog-main', this.element).hide();

  $('input[name=ui-presets-for-field-button]', self.element).click(function() {
    if (! self.dialog) {
      $('.assistant-dialog-main', self.element).show();

      self.dialog = $('.assistant-dialog-main', self.element);
      self.dialog.dialog({
        'width': 800,
        'height': 400,
        'title': self.dialog.children('legend').html(),
        'modal': true
      });
    }
    else {
      self.dialog.dialog('open');
    }
    return false;
  });
};

})(jQuery);
